use full_moon::{
    ast::{
        Stmt,
        punctuated::Punctuated, Expression,
    }, node::{Node, self}, tokenizer::Position 
};
use std::{fs};
use clap::{Parser, Subcommand};

/// A Lua& script packer for the librequire library.
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    #[clap(subcommand)]
    command: Commands,
}

#[derive(Subcommand, Debug)]
enum Commands {
    /// Pack one or multiple Lua files.
    File {
        /// File to be packed.
        #[clap(value_parser)]
        input_file: Option<String>,
        /// Follow the file and pack all files it imports.
        #[clap(long, action)]
        follow_require: bool,
    },
}

fn get_position(node: impl Node) -> (usize, usize, usize, usize) {
    let start_pos = node.start_position().unwrap();
    let end_pos = node.end_position().unwrap();
    (start_pos.line(), start_pos.character(), end_pos.line(), end_pos.character())
}

fn parse_file(input_file: &str, follow_require: bool) -> Result<String, full_moon::Error> {
    let input_file = fs::read_to_string(input_file).unwrap();
    let parsed = full_moon::parse(&input_file)?;
    let stmts = parsed.nodes().stmts();
    for stmt in stmts {
        match stmt {
            Stmt::LocalAssignment(stmt_la) => {
                let names = stmt_la.names();
                for name in names.into_iter() {
                    let name_string = String::from(name.to_string());
                    let name_str = name_string.strip_suffix(" ").unwrap();
                    println!("local variable name: {:?}", name_str);
                    if name_str == "require" {
                        let pos = get_position(name);
                        println!(
                            "local require defined at line:char (start -> end): {:?}:{:?} -> {:?}:{:?}", 
                            pos.0, pos.1, pos.2, pos.3
                        );
                        println!("attempting to remove definition...");
                    }
                }
                let expressions = stmt_la.expressions();
                for expression in expressions.into_iter() {
                    let expression_string = String::from(expression.to_string());
                    let expression_str = expression_string.strip_suffix("\n").unwrap();
                    println!("local expression name: {:?}", expression_str);
                    if expression_string.starts_with("require(") {
                        let pos = get_position(expression);
                        println!(
                            "require called in an local expression at line:char (start -> end): {:?}:{:?} -> {:?}:{:?}", 
                            pos.0, pos.1, pos.2, pos.3
                        );
                        println!("attempting to edit call method...");
                    }
                }
            },
            Stmt::Assignment(stmt_a) => {
                let names = stmt_a.variables();
                for name in names.into_iter() {
                    let name_string = String::from(name.to_string());
                    let name_str = name_string.strip_suffix(" ").unwrap();
                    println!("variable name: {:?}", name_str);
                    if name_str == "require" {
                        let pos = get_position(name);
                        println!(
                            "require defined at line:char (start -> end): {:?}:{:?} -> {:?}:{:?}", 
                            pos.0, pos.1, pos.2, pos.3
                        );
                        println!("attempting to remove definition...");
                    }
                }
                let expressions = stmt_a.expressions();
                for expression in expressions.into_iter() {
                    let expression_string = String::from(expression.to_string());
                    let expression_str = expression_string.strip_suffix("\n").unwrap();
                    println!("local expression name: {:?}", expression_str);
                    if expression_string.starts_with("require(") {
                        let pos = get_position(expression);
                        println!(
                            "require called in an expression at line:char (start -> end): {:?}:{:?} -> {:?}:{:?}", 
                            pos.0, pos.1, pos.2, pos.3
                        );
                        println!("attempting to edit call method...");
                    }
                }
            },
            Stmt::Do(_) => {},
            Stmt::FunctionCall(stmt_fc) => {
                let prefix = stmt_fc.prefix();
                println!("function call: {:?}", prefix.to_string());
                if prefix.to_string() == "require" {
                    let pos = get_position(stmt_fc);
                    println!(
                        "require called at line:char (start -> end): {:?}:{:?} -> {:?}:{:?}", 
                        pos.0, pos.1, pos.2, pos.3
                    );
                    println!("attempting to edit call method...");
                }
            },
            Stmt::FunctionDeclaration(stmt_fd) => {
                let function_name = stmt_fd.name();
                println!("function name: {:?}", function_name.to_string());
                if function_name.to_string() == "require" {
                    let pos = get_position(stmt_fd);
                    println!(
                        "require function defined at line:char (start -> end): {:?}:{:?} -> {:?}:{:?}", 
                        pos.0, pos.1, pos.2, pos.3
                    );
                    println!("attempting to remove definition...");
                }
            },
            Stmt::GenericFor(_) => {},
            Stmt::If(_) => {},
            Stmt::LocalFunction(stmt_lf) => {
                let function_name = stmt_lf.name();
                println!("function name: {:?}", function_name.to_string());
                if function_name.to_string() == "require" {
                    let pos = get_position(stmt_lf);
                    println!(
                        "require local function defined at line:char (start -> end): {:?}:{:?} -> {:?}:{:?}", 
                        pos.0, pos.1, pos.2, pos.3
                    );
                    println!("attempting to remove definition...");
                }
            },
            Stmt::NumericFor(_) => {},
            Stmt::Repeat(_) => {},
            Stmt::While(_) => {},
            Stmt::CompoundAssignment(_) => {},
            Stmt::ExportedTypeDeclaration(_) => {},
            Stmt::TypeDeclaration(_) => {},
            _ => println!("Something else"),
        }
    }
    Ok(full_moon::print(&parsed))
}

fn main() {
    let args = Args::parse();
    match &args.command {
        Commands::File { input_file, follow_require } => {
            println!("Parsing file {:#?}...", input_file);
            println!("Follow require: {:#?}", follow_require);
            let result = parse_file(input_file.as_deref().unwrap(), *follow_require);
            println!("Pack completed successfully");
            println!("{:?}", result.unwrap());
        }
    }
    // let parse_result = full_moon::parse("function hello_world() print('Hello world') end");
    // if parse_result.is_err() {
        // println!("An error occurred while trying to parse Lua code: {}", parse_result.unwrap_err());
        // return;
    // }
    // println!("{:?}", parse_result.unwrap());
}
